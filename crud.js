
let http = require("http");


let directory = [
	{
		"name" : "Brandon",
		"email" : "brandon@mail.com"
	},
	{
		"name" : "Jobert",
		"email" : "jobert@mail.com"
	}
];

http.createServer(function (request, response) {

	// HTTTP Methods:
		/*
			GET, POST, PUT, DELETE
		*/

	// We use 'GET' method to retrieve data
	if(request.url == "/users" && request.method == "GET"){

		// Here we define what content-type we will receive
		response.writeHead(200, {'Content-Type' : 'application/json'});

		response.write(JSON.stringify(directory));
		// ends the response process
		response.end();

	}

	else if(request.url == "/users" && request.method == "POST"){

		// This will act as placeholder for resource/data to be created later on
		let requestBody = ''; 

		/*
			request.on('requestName', function(parameter){
				// code block
			})
		*/

		request.on('data', function(data){ //data is in form of hexadecimals

			console.log(data);

			// this code is where we transform our hexadecimal data to readable text or object
			requestBody += data; 
		});


		request.on('end', function(){

			console.log(requestBody);

			// this transforms our requested data from JSON object to JS object
			requestBody = JSON.parse(requestBody);

			let newUser = {
				// retrieving the field value using dot notation
				"name" : requestBody.name, 
				"email" : requestBody.email
			}

			directory.push(newUser);
			console.log(directory);

			response.writeHead(200, {'Content-Type' : 'application/json'});
			response.write(JSON.stringify(newUser)); 

			response.end();

			// Process inside this code block
			/*
				1. JSON object [from postman]
				2. We retrieved it in a form of hexadecimal [data paremeter]
				3. We transform it to stringified object/JSON object [requestBody += data ]
				4. To access (using dot natation) the fields and values we convert the json object to js object using 'JSON.parse', 
				5. Our response is required to be in a form of string, the reason we stringify the JS object 
			*/
			
		});
	}

}).listen(4000);

console.log("Server running at localhost:4000");


